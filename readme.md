### SQLServ php extension
---

[Descarga](https://gitlab.com/amirtorrez/SQLSrv-PHP/-/archive/master/SQLSrv-PHP-master.zip) o clona el repositorio
```bash
cd Descargas
git clone https://gitlab.com/amirtorrez/SQLSrv-PHP.git
```

[Guía de instalación en español](https://gitlab.com/amirtorrez/SQLSrv-PHP/blob/master/install.[es].md).

Documentación de PDO en español
* http://php.net/manual/es/book.pdo.php
* http://php.net/manual/es/ref.pdo-sqlsrv.connection.php

Fuente:
* https://github.com/Microsoft/msphpsql/releases

---

[Download](https://gitlab.com/amirtorrez/SQLSrv-PHP/-/archive/master/SQLSrv-PHP-master.zip) ot clone the repository
```bash
cd Downloads
git clone https://gitlab.com/amirtorrez/SQLSrv-PHP.git
```

[Installation english guide](https://gitlab.com/amirtorrez/SQLSrv-PHP/blob/master/install.[en].md).


English PDO documentation
* http://php.net/manual/en/book.pdo.php
* http://php.net/manual/en/ref.pdo-sqlsrv.connection.php

Source:
* https://github.com/Microsoft/msphpsql/releases
